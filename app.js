const fs = require('fs');
const child_process = require('child_process');
const http = require('http');

const credentials = {
    server: 'localhost',
    port: 5555
}

try { child_process.exec(`del ${__dirname}\\install.js`, (err1, out1, outerr1) => { });
} catch(err) {}
try { child_process.exec(`del ${__dirname}\\install.vbs`, (err1, out1, outerr1) => { });
} catch(err) {}

let path1 = `C:\\"Program Files (x86)"\\TeamViewer`;
let path2 = `C:\\"Program Files"\\TeamViewer`;

child_process.exec(`dir ${path1}`, (errResult, stdout, stderr) => {
    if (!errResult) child_process.execSync(`setx PATH "${path1.replace(/\"/g, '')};%PATH%"`);
    else {
        child_process.exec(`dir ${path2}`, (errResult2, stdout2, stderr2) => {
            if (!errResult2) child_process.execSync(`setx PATH "${path2.replace(/\"/g, '')};%PATH%"`)
        });
    }
});

http.createServer((request, response) => {
    response.setHeader('Server', 'CoinOp Teamviewer Localhost');
    response.setHeader('X-Powered-By', 'Smart Systems (Armidale)');
    response.setHeader('Access-Control-Allow-Origin', request.headers['origin'] ? request.headers['origin'] : '*');
    response.setHeader('Access-Control-Allow-Credentials', true);
    response.setHeader('Access-Control-Request-Method', '*');
    response.setHeader('Access-Control-Allow-Methods', 'GET, PUT, POST, DELETE, PATCH, OPTIONS');
    response.setHeader('Access-Control-Allow-Headers', 'Content-Type, Authorization');
    response.setHeader('Access-Control-Max-Age', 86400);

    try {
        response.setHeader('Content-Type', 'application/json')
    } catch(errSetContentType){}

    let chunks = []
    let msg = {
        code: 400,
        data: 'Bad Request'
    };

    request.on('data', (chunk) => {
        chunks.push(chunk)
    }).on('error', (errorRequest) => {

    }).on('end', () => {
        if(request['method'].toLowerCase() === 'get') {
            let url = request['url'];

            if(url.startsWith('/cast')) {
                let credentials = {id: '', password: ''};

                try {
                    let splitAuth = (new Buffer.from(request['headers']['authorization'].replace('Basic ', ''), 'base64').toString()).split(':');

                    credentials['id'] = splitAuth[0];
                    credentials['password'] = splitAuth[1];

                    child_process.exec(`TeamViewer.exe -i ${credentials['id']} --Password ${credentials['password']}`, (errCast, stdout4, stderr4) => {

                    });
                    msg['code'] = 200;
                    msg['data'] = 'Teamviewer ID has been casted'
                } catch(errHeader) {}
            } else {
                msg['data'] = "Teamviewer caster is okay";
                msg['code'] = 200
            }
        } else if(request['method'].toLowerCase() === 'options') {
            msg['data'] = "Handshake completed";
            msg['code'] = 200
        }

        response.writeHead(msg['code']);
        response.write(JSON.stringify(msg));
        response.end();
    })
}).listen(credentials['port'])