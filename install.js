const child_process = require('child_process');

let path1 = `C:\\"Program Files (x86)"\\TeamViewer`;
let path2 = `C:\\"Program Files"\\TeamViewer`;

child_process.exec(`dir ${path1}`, (errResult, stdout, stderr) => {
    if (!errResult) child_process.execSync(`setx PATH "${path1.replace(/\"/g, '')};%PATH%"`);
    else {
        child_process.exec(`dir ${path2}`, (errResult2, stdout2, stderr2) => {
            if (!errResult2) child_process.execSync(`setx PATH "${path2.replace(/\"/g, '')};%PATH%"`)
        });
    }
});